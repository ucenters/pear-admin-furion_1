using Furion.DatabaseAccessor;
using System;
using System.ComponentModel.DataAnnotations;

namespace Pear.Core
{
    /// <summary>
    /// API 请求日志
    /// </summary>
    public class LogApi : Entity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public LogApi()
        {

        }


        /// <summary>
        /// 日志状态
        /// </summary>
        public LogStatus LogStatus { get; set; }

        /// <summary>
        /// 访问URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 查询参数
        /// </summary>
        public string QueryParams { get; set; }

        /// <summary>
        /// 表单参数
        /// </summary>
        public string FormParams { get; set; }

        /// <summary>
        /// 返回结果
        /// </summary>
        public string Result { get; set; }
 
        /// <summary>
        /// IP地址
        /// </summary>
        public string IpAddress { get; set; }
        /// <summary>
        /// IP所属地
        /// </summary>
        public string IpLocation { get; set; }
        /// <summary>
        /// 浏览器
        /// </summary>
        public string Browser { get; set; }
        /// <summary>
        /// 操作系统
        /// </summary>
        public string OS { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
 
        /// <summary>
        /// 请求时间
        /// </summary>
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// 登录用户编号
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// 登录用户
        /// </summary>
        public User User { get; set; }
 
        /// <summary>
        /// 执行时间 ms
        /// </summary>
        public int ExecutionTime  { get; set; }
    }
}