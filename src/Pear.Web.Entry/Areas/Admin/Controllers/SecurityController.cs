﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 权限-后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class SecurityController : Controller
    {
        /// <summary>
        /// 权限菜单-主视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.security:view")]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 权限菜单新增-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.security:add")]
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 权限菜单编辑-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.security:edit")]
        public IActionResult Edit()
        {
            return View();
        }





    }
}
