﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 部门-后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class DepartmentController : Controller
    {
        /// <summary>
        /// 部门管理主视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.usercenter.dept:view")]
        public IActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 权限菜单新增-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.usercenter.dept:add")]
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 权限菜单编辑-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.usercenter.dept:edit")]
        public IActionResult Edit()
        {
            return View();
        }
    }
}
