﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 工具-后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class ToolController : Controller
    {
        /// <summary>
        /// 系统工具-表单设计器-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.tool.formdev:view")]
        public IActionResult FormDev()
        {
            return View();
        }

 
    }
}
